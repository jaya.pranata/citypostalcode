//
//  AddCityViewController.swift
//  RentalMovie
//
//  Created by Jaya Pranata on 7/8/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import UIKit

class AddCityViewController: UIViewController {
    @IBOutlet weak var cityNameTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addCityButtonClick(_ sender: UIButton) {
        performSegue(withIdentifier: "backToCityList", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
