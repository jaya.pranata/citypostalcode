//
//  CityListTableViewController.swift
//  RentalMovie
//
//  Created by Jaya Pranata on 7/8/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import UIKit

class CityListTableViewController: UITableViewController {
    var cities: [City]?
    var cityManager = CityManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        cities = cityManager.cities
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cities?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //selectedPostalCode = cities[]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postalCodes", for: indexPath)
        // Configure the cell...
        cell.textLabel?.text = (cities?[indexPath.row].name)! + "(\(cities![indexPath.row].PostalCode!.joined(separator: ",")))"
        
        return cell
    }
    @IBAction func sortItemClick(_ sender: Any) {
        cities = cityManager.sortCity()
        tableView.reloadData()
    }
    
    @IBAction func unwindToCityList( _ seg: UIStoryboardSegue) {
        let source = seg.source as! AddCityViewController
        cities =  cityManager.insertCity(name: source.cityNameTextField.text!, PostalCodeString: source.postalCodeTextField.text!)
        tableView.reloadData()
    }
    
}
