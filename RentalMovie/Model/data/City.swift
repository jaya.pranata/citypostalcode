//
//  City.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

class City :NSObject, NSCoding {
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id");
        coder.encode(self.name, forKey: "name");
        coder.encode(self.PostalCode, forKey: "PostalCode");
    }
    
    required init?(coder: NSCoder) {
        self.id = coder.decodeObject(forKey: "id") as? Int
        self.name = coder.decodeObject(forKey: "name") as? String
        self.PostalCode = coder.decodeObject(forKey: "PostalCode") as? [String]
    }
    init(id:Int, name:String, PostalCode:[String]) {
        self.id = id
        self.name = name
        self.PostalCode = PostalCode
    }
    
    let id: Int?
    let name: String?
    let PostalCode: [String]?
}
