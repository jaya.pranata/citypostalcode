//
//  CityManager.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

class CityManager {
    var cities: [City]
    var sortDirection:Bool = true
    init() {
        cities = [City]()
        if let decoded  = UserDefaults.standard.data(forKey: "cities"){
            if let myCity = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [City]{
                cities = myCity
            }
        }
    }
    func insertCity(name:String, PostalCodeString:String)->[City]{
        let result = PostalCodeString.filter { !$0.isWhitespace }
        let PostalCode = result.components(separatedBy: ",")
        let id = cities.count + 1
        cities.append(City(id: id, name: name, PostalCode: PostalCode))
        let encodedData: Data = try! NSKeyedArchiver.archivedData(withRootObject: cities, requiringSecureCoding: false)

        UserDefaults.standard.set(encodedData, forKey: "cities")
        return cities
    }
    
    func sortCity()->[City]{
        if sortDirection{
            cities.sort {
                $0.name! > $1.name!
            }
        }else{
            cities.sort {
                $0.name! < $1.name!
            }
        }
        
        sortDirection = !sortDirection
        return cities
    }
}
